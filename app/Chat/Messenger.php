<?php

namespace App\Chat;

use Pusher\Pusher;

class Messenger
{
    protected $pusher;
    protected $channel;
    protected $event;

    public function __construct(string $channel, string $event)
    {
        $this->channel = $channel;
        $this->event = $event;

        $appId = config('pusher.app_id');
        $appKey = config('pusher.key');
        $appSecret = config('pusher.secret');
        $appCluster = config('pusher.options.cluster');

        $this->pusher = new Pusher($appKey, $appSecret, $appId, ['cluster' => $appCluster]);
    }

    public function  __call($method, $params)
    {
        if (method_exists($this->pusher, $method)) {
            return call_user_func_array([$this->pusher, $method], $params);
        }
    }

    public function send(array $message)
    {
        $this->pusher->trigger($this->channel, $this->event, $message);
    }
}
