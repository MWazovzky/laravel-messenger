<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Chat\Messenger;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{
    public function create(Request $request)
    {
        $message = [
            'message' => $request->message,
            'user' => Auth::user(),
        ];

        $messenger = new Messenger('test-channel', 'test-event');

        $messenger->send($message);

        return response()->json($message);
    }
}
