import { debounce } from '../helpers'

export const FitScreenHeight = {
    bind: function(el, binding, vNode) {
        const handler = (e) => {
            const defaultMarginBottom = 10
            const marginBottom = binding.value && binding.value.marginBottom || defaultMarginBottom
            let top = el.getBoundingClientRect().top
            let screenHeight = window.innerHeight
            let containerHeigth = screenHeight - top - marginBottom
            el.style.height = `${containerHeigth}px`
        }

        el.resizeContainerHandler = debounce(handler, 100) 
        window.addEventListener('resize', el.resizeContainerHandler)
    },

    inserted: function(el, binding) {
        el.resizeContainerHandler()
    },

    unbind: function(el, binding) {
        window.removeEventListener('resize', el.resizeContainerHandler)
        el.resizeContainerHandler = null
    }
}
