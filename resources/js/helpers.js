function debounce (func, wait = 200) {
    let timeout

    return function () {
        let args = arguments
        let later = () => {
            timeout = null
            func.apply(this, args)
        }

        clearTimeout(timeout)
        timeout = setTimeout(later, wait)
    }
}

function throttle (func, wait) {
    let timeout

    return function() {
        let args = arguments
        let later = () => {
            timeout = null
        }
      
        if (!timeout) {
            func.apply(this, args)
            timeout = setTimeout(later, wait)
        }
    }
}

export { debounce, throttle }
