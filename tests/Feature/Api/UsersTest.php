<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function it_can_get_user()
    {
        $this->withoutExceptionHandling();
        
        $user = factory('App\User')->create(['name' => 'John']);
        $this->actingAs($user, 'api');

        $response = $this->get("/api/user");

        $response
            ->assertStatus(200)
            ->assertJson([
                'name' => 'John',
            ]);
    }
}
